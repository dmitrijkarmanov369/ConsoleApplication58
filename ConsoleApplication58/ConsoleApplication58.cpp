﻿#include <iostream>
#include <ctime>
using namespace std;

typedef unsigned int uint;

uint sumBet(uint& a, uint& b);

int main()
{
	setlocale(LC_ALL, "ru");

	uint a;
	uint b;

	cout << "Введите первое число: ";
	cin >> a;
	cout << endl;
	cout << "Введите второе чило: ";
	cin >> b;
	cout << endl;

	uint result = sumBet(a, b);

	cout << "\tРезультат суммы между введеными числами: " << result;

	cout << endl; cout << endl;
	return 0;
}

uint sumBet(uint& a, uint& b) {

	uint res = 0;

	if (a == b)
	{
		cout << "\tЧисла равны!" << endl << endl;
		return 0;
	}
	else
	{
		for (uint i = a + 1; i < b; i++)
		{
			res += i;
		}
		return res;
	}
}